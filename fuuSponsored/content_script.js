function addToggleEl(spamEl)
{
    var toggleLink = $('<a>', {
        text: 'Toggle sponsored',
        href: '#',
        click: function(){
            spamEl.toggle();
            return false;
        }
    });
    toggleLink.insertAfter(spamEl);
}
function hideTimelineSponsors()
{
    var sponsorEls = $("span.uiStreamAdditionalLogging:visible");
	
    sponsorEls.each(function(index, domEl){
		
	var spamEl = $(domEl);
	
	var sponsoredDepth=-1;
	for(var i=0; i<20; i++)
	{
	    if(spamEl.get(0).tagName == 'LI'){
		if($(spamEl.get(0)).hasClass('uiStreamStory'))
		{
		    sponsoredDepth=i;
		    break;
		}
	    }
	    spamEl = spamEl.parent();
	}
	if(sponsoredDepth == -1)
	{
	    console.log('could not catch spamEl');
	    return;
	}
	addToggleEl(spamEl);
        spamEl.hide();
    });
    
}

function hideEgoSponsors()
{
    var egoColumn = $('div.ego_column:visible');
    egoColumn.each(function(index, domEl){
	var spamEl = $(domEl);
	if(spamEl.attr('class') != 'ego_column') {
	    return;
	}
	addToggleEl(spamEl);
	spamEl.hide();
    });
}

$(document).ready(function() {
    var timelineElements = $("#home_stream").children().size()
    document.numTimelineElements = timelineElements;
    hideEgoSponsors();
    hideTimelineSponsors();
    setInterval(function(){
	var timelineElements = $("#home_stream").children().size()
	if(timelineElements != document.numTimelineElements)
	{
	    hideTimelineSponsors();
	    document.numTimelineElements = timelineElements;
	}
	hideEgoSponsors();
    },500);
});
